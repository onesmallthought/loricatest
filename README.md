# loricatest

This is a solution to the problem provided by Lorica Health

Please run 'Application' from the com.loricahealth package to begin the command line

Classes of significance:

- Date 
- Event
- EventRunner 

I felt there needed to be a separation of concern between what is the date model (known 'facts' about dates) and the business logic (event-specific logic, e.g. cannot be earlier than 01/01/1901). I've used the EventRunner to house this, although if I had more time, I would consider a better name for this class
