package com.loricahealth;

import com.loricahealth.domain.EventRunner;
import com.loricahealth.system.CommandLine;

public class Application {

    public static void main(String[] args) throws Exception {
        // Setup logger... prefer SLF4J, but not permitted to use external frameworks
        System.setProperty("java.util.logging.SimpleFormatter.format",
                "[%1$tF %1$tT] [%4$-5s] [%3$s] %5$s %n");

        // Start the command line: inject Event Runner as the Command Line Runner
        new CommandLine(new EventRunner()).start();
    }

}
