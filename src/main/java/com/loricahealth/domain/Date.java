package com.loricahealth.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static java.lang.Integer.parseInt;

public class Date {

    private static final long EPOCH = 0;

    private static final Map<Integer, Function<Integer, Integer>> MONTH_DAY_RANGE = new HashMap<Integer, Function<Integer, Integer>>() { {
        put(1, (year) -> 31);
        put(2, (year) -> isLeapYear(year) ? 29 : 28);
        put(3, (year) -> 31);
        put(4, (year) -> 30);
        put(5, (year) -> 31);
        put(6, (year) -> 30);
        put(7, (year) -> 31);
        put(8, (year) -> 31);
        put(9, (year) -> 30);
        put(10, (year) -> 31);
        put(11, (year) -> 30);
        put(12, (year) -> 31);
    } };

    public final int day;
    public final int month;
    public final int year;

    final int daysSinceEpoch;

    // Only accepts dates in the format DD/MM/YYYY
    public static boolean isValidDate(String dateString) throws DateFormatException {
        String [] dateBits = dateString.split("/");
        if (dateBits.length != 3) {
            throw new DateFormatException("Date must contain numeric values in the format: DD/MM/YYYY");
        }
        try {
            int year = verifyYear(parseInt(dateBits[2]));
            int month = verifyMonth(parseInt(dateBits[1]));
            int day = verifyDay(parseInt(dateBits[0]), month, year);
        } catch (NumberFormatException nfe) {
            throw new DateFormatException("Date must contain numeric values in the format: DD/MM/YYYY");
        }
        return true;
    }

    // Only accepts dates in the format DD/MM/YYYY
    public static Date parse(String dateString) throws DateFormatException {
        isValidDate(dateString);
        String [] dateBits = dateString.split("/");
        return new Date(parseInt(dateBits[0]), parseInt(dateBits[1]), parseInt(dateBits[2]));
    }

    // =====================================================================
    // Logic
    public int daysDifferentTo(Date date) {
        return Math.abs(this.daysSinceEpoch - date.daysSinceEpoch);
    }
    public boolean isBeforeOrEqualTo(Date date) {
        return this.daysSinceEpoch - date.daysSinceEpoch < 0;
    }
    public boolean isAfterOrEqualTo(Date date) {
        return this.daysSinceEpoch - date.daysSinceEpoch > 0;
    }

    // =====================================================================
    // Validation
    private static int verifyYear(int year) throws DateFormatException {
        if (year < 1) {
            throw new DateFormatException("A date's Year value must be greater than zero");
        }
        return year;
    }

    private static int verifyMonth(int month) throws DateFormatException {
        if (month < 1 || month > 12) {
            throw new DateFormatException("A date's Month value must be between 1 and 12 inclusive");
        }
        return month;
    }

    private static int verifyDay(int day, int month, int year) throws DateFormatException {
        if (day < 0) {
            throw new DateFormatException("A date's Day value must be a value greater than zero");
        }
        if (MONTH_DAY_RANGE.get(month).apply(year) < day) {
            throw new DateFormatException("A date's Day value of " + day + " must be within the max of " +
                    MONTH_DAY_RANGE.get(month).apply(year) + " days for the month of " + month);
        }
        return day;
    }

    private static boolean isLeapYear(int year) {
        return year % 4 == 0;
    }

    // =====================================================================
    // Constructor
    private Date(int day, int month, int year) throws RuntimeException {
        if (day < 0 || month < 0 || year < 0) {
            throw new RuntimeException("Something awful has happened");
        }
        this.day = day;
        this.month = month;
        this.year = year;
        // estimate days since epoch
        this.daysSinceEpoch = (((this.year - 1) / 4) * 366) +
                (((this.year - 1) - ((this.year - 1) / 4)) * 365) + daysSinceMonth(month, year) + day;
    }

    private int daysSinceMonth(int month, int year) {
        int daysSinceMonth = 0;
        for (int i = 1; i < month; i++) {
            daysSinceMonth += MONTH_DAY_RANGE.get(i).apply(year);
        }
        return daysSinceMonth;
    }

    @Override
    public String toString() {
        return (day < 10 ? "0" + day : day) + "/" + (month < 10 ? "0" + month : month) + "/" +
                (year < 10 ? "000" + year : year < 100 ? "00" + year : year < 1000 ? "0" + year : year);
    }
}
