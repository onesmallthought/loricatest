package com.loricahealth.domain;

public class DateFormatException extends Exception {

    private static final long serialVersionUID = 1L;

    public DateFormatException() {
    }

    public DateFormatException(String message) {
        super(message);
    }
}
