package com.loricahealth.domain;

import static com.loricahealth.domain.Date.isValidDate;

public class Event {

    public final Date fromDate;
    public final Date untilDate;

    public static Event parse(String eventString) throws EventParseException, DateFormatException {
        checkValidEventEntry(eventString);
        checkValidDateFormat(eventString);

        String [] dateStrings = eventString.split(" ");

        // should an event have the first date 'before' the last date?  Maybe.. but there's no requirement for this.
        return new Event(Date.parse(dateStrings[0]), Date.parse(dateStrings[1]));
    }

    public static void checkValidEventEntry(String eventString) throws EventParseException {
        if (isEmpty(eventString)) {
            throw new EventParseException("Event cannot be an empty value");
        }
        if (eventString.split(" ").length != 2) {
            throw new EventParseException("Event requires two dates separated by a single space");
        }
        if (eventString.length() != 21) {
            throw new EventParseException("Event requires two dates in the format: DD/MM/YYYY DD/MM/YYYY");
        }
    }

    public static void checkValidDateFormat(String eventString) throws EventParseException, DateFormatException {
        if (isEmpty(eventString)) {
            throw new EventParseException("Event cannot be an empty value");
        }
        String [] dates = eventString.split(" ");
        if (dates.length != 2) {
            throw new EventParseException("Event requires two dates separated by a single space");
        }
        if (isValidDate(dates[0]) && isValidDate(dates[1])) {
            return;
        }
        // default catch error in case something bad went wrong
        throw new DateFormatException("Invalid date format specified");
    }

    private static boolean isEmpty(String value) {
        return value == null || value.trim().length() == 0;
    }

    private Event(Date fromDate, Date untilDate) {
        this.fromDate = fromDate;
        this.untilDate = untilDate;
    }
}
