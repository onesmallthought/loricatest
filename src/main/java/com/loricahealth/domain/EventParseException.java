package com.loricahealth.domain;

public class EventParseException extends Exception {

    private static final long serialVersionUID = 1L;

    public EventParseException() {
    }

    public EventParseException(String message) {
        super(message);
    }
}
