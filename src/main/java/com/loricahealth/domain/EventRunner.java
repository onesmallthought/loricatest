package com.loricahealth.domain;

import com.loricahealth.system.CommandLineRunner;

import static java.lang.String.format;

public class EventRunner implements CommandLineRunner {

    private static final String INVALID_EVENT_ERROR = "Invalid event: %s";
    private static final String VALID_DAYS_RANGE = "%d";

    private final Date LOWER_DATE_BOUNDARY;
    private final Date UPPER_DATE_BOUNDARY;

    public EventRunner() throws Exception {
        this.UPPER_DATE_BOUNDARY = Date.parse("31/12/2999");
        this.LOWER_DATE_BOUNDARY = Date.parse("01/01/1901");
    }

    @Override
    public String welcomeMessage() {
        return "***************************\n" +
               " Event Parser Online \\o/\n" +
               " (type 'exit' to quit)\n" +
               "***************************";
    }

    @Override
    public String prompt() {
        return "Please enter two dates in the format: DD/MM/YYYY DD/MM/YYYY";
    }

    @Override
    public String received(String eventString) {
        eventString = eventString.trim();
        try {
            Event event = Event.parse(eventString);
            // since our runner requires events between a specific range, validate them here.
            verifyBoundaryDates(event.fromDate, event.untilDate);

            int daysDifference = event.untilDate.daysDifferentTo(event.fromDate);
            // since our runner requires 'exclusive' dates difference, we subtract 1
            daysDifference = daysDifference - 1;

            return format(VALID_DAYS_RANGE, daysDifference);
        } catch (DateFormatException | EventParseException | InvalidEventException e) {
            return format(INVALID_EVENT_ERROR, e.getMessage());
        }
    }

    private void verifyBoundaryDates(Date fromDate, Date untilDate) throws InvalidEventException {
        if (LOWER_DATE_BOUNDARY.isAfterOrEqualTo(fromDate)) {
            throw new InvalidEventException("The date " + fromDate.toString() + " requires to be after the date " + LOWER_DATE_BOUNDARY);
        }
        if (LOWER_DATE_BOUNDARY.isAfterOrEqualTo(untilDate)) {
            throw new InvalidEventException("The date " + untilDate.toString() + " requires to be after the date " + LOWER_DATE_BOUNDARY);
        }
        if (UPPER_DATE_BOUNDARY.isBeforeOrEqualTo(untilDate)) {
            throw new InvalidEventException("The date " + untilDate.toString() + " requires to be before the date " + UPPER_DATE_BOUNDARY);
        }
        if (UPPER_DATE_BOUNDARY.isBeforeOrEqualTo(fromDate)) {
            throw new InvalidEventException("The date " + fromDate.toString() + " requires to be before the date " + UPPER_DATE_BOUNDARY);
        }
    }

}



