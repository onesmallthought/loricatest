package com.loricahealth.domain;

public class InvalidEventException extends Exception {

    private static final long serialVersionUID = 1L;

    public InvalidEventException() {
    }

    public InvalidEventException(String message) {
        super(message);
    }
}


