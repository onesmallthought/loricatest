package com.loricahealth.system;

import java.util.Scanner;
import java.util.logging.Logger;

public class CommandLine {

    private static final Logger LOG = Logger.getLogger(CommandLine.class.getName());
    private static final String EXIT_COMMAND = "exit";

    private CommandLineRunner runner;

    public CommandLine(CommandLineRunner runner) {
        if (runner == null) {
            LOG.severe("Command line started without a runner, exiting");
            System.exit(0);
        }
        this.runner = runner;
    }

    public void start() {
        System.out.println(runner.welcomeMessage());
        Scanner scanner = new Scanner(System.in);
        String input;
        System.out.println(runner.prompt());
        while (scanner.hasNextLine() && !(input = scanner.nextLine()).equalsIgnoreCase(EXIT_COMMAND)) {
            System.out.println(runner.received(input));
            System.out.println(runner.prompt());
        }
    }

}
