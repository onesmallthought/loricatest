package com.loricahealth.system;

// You could really give this interface an 'abstract' name
public interface CommandLineRunner {

    String welcomeMessage();

    String prompt();

    String received(String command);

}
