package com.loricahealth.domain;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;

public class DateTest {

    // Parsing
    @Test
    public void shouldParseCorrectlyFormattedDate() throws DateFormatException {
        assertThat(Date.parse("01/01/2000"), is(notNullValue()));
    }

    @Test(expected = DateFormatException.class)
    public void shouldNotParseIncorrectlyFormattedDate() throws DateFormatException {
        Date.parse("01-01-2000");
    }

    @Test(expected = DateFormatException.class)
    public void shouldNotParseDateWithMonthOutOfRange() throws DateFormatException {
        Date.parse("01/21/2000");
    }

    @Test(expected = DateFormatException.class)
    public void shouldNotParseDateWithDayOutOfRange() throws DateFormatException {
        Date.parse("41/01/2000");
    }

    @Test(expected = DateFormatException.class)
    public void shouldNotParseDateWithDayOutOfRangeInNonLeapYear() throws DateFormatException {
        Date.parse("29/02/1999");
    }

    @Test(expected = DateFormatException.class)
    public void shouldNotParseDateWithNegativeDayValues() throws DateFormatException {
        Date.parse("-9/02/1999");
    }

    // Date comparison
    @Test
    public void shouldHaveEarlierDateVerifiedAsBeforeLaterDate() throws DateFormatException {
        Date earlierDate = Date.parse("01/01/2000");
        Date laterDate = Date.parse("02/01/2000");
        assertThat(earlierDate.isBeforeOrEqualTo(laterDate), equalTo(true));
    }

    @Test
    public void shouldNotHaveLaterDateVerifiedAsBeforeEarlierDate() throws DateFormatException {
        Date earlierDate = Date.parse("01/01/2000");
        Date laterDate = Date.parse("02/01/2000");
        assertThat(laterDate.isBeforeOrEqualTo(earlierDate), equalTo(false));
    }

    @Test
    public void shouldHaveLaterDateVerifiedAsAfterEarlierDate() throws DateFormatException {
        Date earlierDate = Date.parse("01/01/2000");
        Date laterDate = Date.parse("02/01/2000");
        assertThat(laterDate.isAfterOrEqualTo(earlierDate), equalTo(true));
    }
    @Test
    public void shouldNotHaveEarlierDateVerifiedAsAfterLaterDate() throws DateFormatException {
        Date earlierDate = Date.parse("01/01/2000");
        Date laterDate = Date.parse("02/01/2000");
        assertThat(earlierDate.isAfterOrEqualTo(laterDate), equalTo(false));
    }

    // We keep the days between dates as not 'inclusive' of the upper/lower bound date
    @Test
    public void shouldCountOneDayBetweenDatesWhereEarlierAndLaterDateAreDifferent() throws DateFormatException {
        Date earlierDate = Date.parse("01/01/2000");
        Date laterDate = Date.parse("02/01/2000");
        assertThat(earlierDate.daysDifferentTo(laterDate), equalTo(1));
    }

    @Test
    public void shouldCountZeroDaysBetweenDatesWhereTwoDatesAreTheSame() throws DateFormatException {
        Date earlierDate = Date.parse("01/01/2000");
        Date laterDate = Date.parse("01/01/2000");
        assertThat(earlierDate.daysDifferentTo(laterDate), equalTo(0));
    }

}