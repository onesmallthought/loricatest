package com.loricahealth.domain;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.*;

public class EventRunnerTest {

    @Test
    public void shouldHaveOneDayDifferenceForEventWithCorrectDateFormatted() throws Exception {
        EventRunner eventRunner = new EventRunner();

        assertThat(eventRunner.received("01/01/2000 03/01/2000"), equalTo("1"));
    }

    @Test
    public void shouldHaveZeroDaysDifferenceForEventWithDatesOneDayApart() throws Exception {
        EventRunner eventRunner = new EventRunner();

        assertThat(eventRunner.received("01/01/2000 02/01/2000"), equalTo("0"));
    }

    @Test
    public void shouldNotAllowDatesAboveUpperThreshold() throws Exception {
        String upperThresholdExceeded = "01/01/3000";
        EventRunner eventRunner = new EventRunner();

        assertThat(eventRunner.received("01/01/2000 " + upperThresholdExceeded), startsWith("Invalid event"));
    }

    @Test
    public void shouldNotAllowDatesBelowLowerThresholdA() throws Exception {
        String lowerThresholdExceeded = "31/12/1900";
        EventRunner eventRunner = new EventRunner();

        assertThat(eventRunner.received(lowerThresholdExceeded + " " + lowerThresholdExceeded), startsWith("Invalid event"));
    }

    @Test
    public void shouldHaveNineteenDaysDifferenceWithSampleTestDateRange() throws Exception {
        EventRunner eventRunner = new EventRunner();

        assertThat(eventRunner.received("02/06/1983 22/06/1983"), equalTo("19"));
    }

}