package com.loricahealth.domain;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;

public class EventTest {

    @Test(expected = EventParseException.class)
    public void shouldNotCreateEventWhenMissingDate() throws EventParseException, DateFormatException {
        Event.parse("01/01/2000");
    }
    @Test(expected = EventParseException.class)
    public void shouldNotHaveValidEventEntryWhenMissingDate() throws EventParseException {
        Event.checkValidEventEntry("01/01/2000");
    }

    @Test(expected = EventParseException.class)
    public void shouldNotCreateEventWhenMissingSpace() throws EventParseException, DateFormatException {
        Event.parse("01/01/2000,01/02/2000");
    }
    @Test(expected = EventParseException.class)
    public void shouldNotHaveValidEventEntryWhenMissingSpace() throws EventParseException {
        Event.checkValidEventEntry("01/01/2000,01/02/2000");
    }

    @Test(expected = DateFormatException.class)
    public void shouldNotCreateEventWhenBadlyFormattedDate() throws EventParseException, DateFormatException {
        Event.parse("01/01/2000 01-02-2000");
    }

    @Test(expected = DateFormatException.class)
    public void shouldNotCreateEventWhenMonthOutOfBounds() throws EventParseException, DateFormatException {
        Event.parse("01/01/2000 01/13/2000");
    }

    @Test
    public void shouldCreateEventWhenCorrectlyFormattedDatesGiven() throws EventParseException, DateFormatException {
        assertThat(Event.parse("01/01/2000 02/01/2000"), is(notNullValue()));
    }

}